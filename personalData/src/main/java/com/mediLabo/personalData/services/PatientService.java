package com.mediLabo.personalData.services;

import com.mediLabo.personalData.entities.Patient;
import com.mediLabo.personalData.forms.PatientForm;
import com.mediLabo.personalData.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PatientService {

  @Autowired
  private PatientRepository patientRepository;

  public Optional<Patient> getPatientData(PatientForm patientForm) {
    return patientRepository.getPatientsByFirstNameAndLastName(patientForm);
  }
  public Patient getPatientDataById(int patientId) {
    return patientRepository.getReferenceById(patientId);
  }

  public List<Patient> getPatientsData() {
    return patientRepository.findAll();
  }

  public void deletePatientData(Integer id) {
    patientRepository.deleteById(id);
  }

  public void updatePatientData(Patient patient) {
    patientRepository.updatePatientData(patient);
  }

  public void addPatientData(Patient patient) {
    patientRepository.save(patient);
  }
}
