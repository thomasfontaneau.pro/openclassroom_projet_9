package com.mediLabo.personalData.repositories;

import com.mediLabo.personalData.entities.Patient;
import com.mediLabo.personalData.forms.PatientForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

  @Query("SELECT pa FROM Patient pa WHERE pa.firstName ILIKE :#{#patientForm.firstName} AND pa.lastName ILIKE :#{#patientForm.lastName}")
  Optional<Patient> getPatientsByFirstNameAndLastName(PatientForm patientForm);

  @Modifying
  @Query("UPDATE Patient pa set pa.firstName = :#{#patientData.firstName}, pa.lastName = :#{#patientData.lastName}, pa.birthday = :#{#patientData.birthday}, pa.gender = :#{#patientData.gender}, pa.postaleAddress = :#{#patientData.postaleAddress}, pa.phoneNumber = :#{#patientData.phoneNumber} WHERE pa.id = :#{#patientData.id}")
  void updatePatientData(Patient patientData);
}
