package com.mediLabo.personalData.controllers;

import com.mediLabo.personalData.entities.Patient;
import com.mediLabo.personalData.forms.PatientForm;
import com.mediLabo.personalData.services.PatientService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;

@Controller
public class PatientController {

  @Autowired
  private PatientService patientService;

  @PostMapping("/patient/data")
  public ResponseEntity<?> getPatientData(@Valid @RequestBody PatientForm patientForm, BindingResult result, Model model) {
    Optional<Patient> patientData = patientService.getPatientData(patientForm);

    if (patientData.isPresent()) {
      return new ResponseEntity<>(patientData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>("Pas de patient trouvé", HttpStatus.NOT_FOUND);
    }
  }

  @ResponseBody
  @GetMapping("/patient/data/{id}")
  public Patient getPatientDataById(@PathVariable("id") Integer id) {
    return patientService.getPatientDataById(id);
  }

  @GetMapping("/patient/data")
  @ResponseBody
  public List<Patient> getPatientsData() {
    return patientService.getPatientsData();
  }

  @DeleteMapping("/patient/delete/{id}")
  public ResponseEntity<?> deletePatientData(@PathVariable("id") Integer id) {
    patientService.deletePatientData(id);
    return new ResponseEntity<>(null, HttpStatus.OK);
  }

  @PostMapping("/patient/update")
  public ResponseEntity<?> updatePatientData(@Valid @RequestBody Patient patient) {
    patientService.updatePatientData(patient);
    return new ResponseEntity<>(null, HttpStatus.OK);
  }

  @PostMapping("/patient/add")
  @ResponseBody
  public ResponseEntity<?> addPatientData(@Valid @RequestBody Patient patient) {
    patientService.addPatientData(patient);
    return new ResponseEntity<>(null, HttpStatus.OK);
  }
}
