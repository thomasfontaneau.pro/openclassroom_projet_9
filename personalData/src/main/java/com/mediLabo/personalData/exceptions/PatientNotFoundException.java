package com.mediLabo.personalData.exceptions;

public class PatientNotFoundException extends Throwable{
  public PatientNotFoundException(String s){
    super(s);
  }
}
