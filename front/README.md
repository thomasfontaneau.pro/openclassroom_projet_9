# MédiLabo Solutions

Ce projet vise à créer une application pour permettre le suivi et la surveillance des patients. L'application regroupe un système de note, de stockage des données des patients ainsi qu'un système de comptes rendus rendus qui en fonction de différents paramètres alertes sur l'état de santé d'un patient.

### Architecture

Le projet se divise en micro-service qui communique de la manière suivante : 

![projet_9_achitecture.jpg](src%2Fmain%2Fresources%2Fimg%2Fprojet_9_achitecture.jpg)

Avec le front qui gére la partie affichage des données et qui va interroger la gateway pour récupérer les différentes informations.
Ensuite la Gateway aura pour rôle d'interroger le bon micro-service pour récupérer les données et les retourner au front.

### Mise en place

Lancer un terminal dans le project front et exécuter la commande suivante :

```bash
docker compose up
```

Vous devriez obtenir un projet avec les 5 images des micro-services ainsi que les deux bases de données utilisées (Mysql et MongoDB) :

![Capture d’écran 2024-05-24 à 15.47.15.png](src%2Fmain%2Fresources%2Fimg%2FCapture%20d%E2%80%99%C3%A9cran%202024-05-24%20%C3%A0%2015.47.15.png)

Vous pouvez ensuite accéder à l'application avec l'address :
http://localhost:8084/

### GreenCode

Le green code est une manière de coder qui vise à réduire l'impact de l'application sur l'environnement. Cela peut passer par plusieurs points :

- Mise en place de serveur qui consomme moins d'électricité.
- Un code optimisé pour éviter les boucles ou la logique inutile.
- Une réduction des appels HTTP.
- Pour la partie front évité d'utiliser quand c'est possible des scripts Javascript.