CREATE TABLE IF NOT EXISTS patient_data (
    id int NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    birthday DATE NOT NULL,
    gender CHAR NOT NULL,
    postal_address VARCHAR(255),
    phone_number VARCHAR(12),

    PRIMARY KEY (id)
);

DELETE FROM patient_data;

INSERT INTO patient_data (id, first_name, last_name, birthday, gender, postal_address, phone_number)
VALUES (1, 'TestNone', 'Test', '1966-12-31', 'F', '1 Brookside St', '100-222-3333');

INSERT INTO patient_data (id, first_name, last_name, birthday, gender, postal_address, phone_number)
VALUES (2, 'TestBorderline', 'Test', '1945-06-24', 'M', '2 High St', '200-333-4444');

INSERT INTO patient_data (id, first_name, last_name, birthday, gender, postal_address, phone_number)
VALUES (3, 'TestInDanger', 'Test', '2004-06-18', 'M', '3 Club Road', '300-444-5555');

INSERT INTO patient_data (id, first_name, last_name, birthday, gender, postal_address, phone_number)
VALUES (4, 'TestEarlyOnset', 'Test', '2002-06-28', 'F', '4 Valley Dr', '400-555-6666');
