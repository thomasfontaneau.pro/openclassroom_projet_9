package com.medilabo.front.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class Note {
  private String id;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private LocalDate date;

  private Integer patientId;

  private String note;

  public Note(int patientId, String note) {
    this.id = "";
    this.date = LocalDate.now();
    this.patientId = patientId;
    this.note = note;
  }
}