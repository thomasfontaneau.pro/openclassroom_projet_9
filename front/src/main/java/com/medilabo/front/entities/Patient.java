package com.medilabo.front.entities;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class Patient {
  private Integer id;

  private String firstName;

  private String lastName;

  private LocalDate birthday;

  private char gender;

  private String postaleAddress;

  private String phoneNumber;
}
