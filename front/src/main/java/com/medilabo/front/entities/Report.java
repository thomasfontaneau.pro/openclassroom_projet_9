package com.medilabo.front.entities;

public enum Report {
  NONE, BORDERLINE, IN_DANGER, EARLY_ONSET
}