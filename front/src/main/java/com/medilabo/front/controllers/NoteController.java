package com.medilabo.front.controllers;

import com.medilabo.front.entities.Note;
import com.medilabo.front.entities.Patient;
import com.medilabo.front.services.NoteService;

import com.medilabo.front.services.PatientService;
import com.medilabo.front.services.ReportService;
import jakarta.validation.Valid;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NoteController {
  @Autowired
  private NoteService noteService;

  @Autowired
  private PatientService patientService;

  @Autowired
  private ReportService reportService;

  @RequestMapping("/notes/{id}")
  public String homeNote(Model model, @PathVariable Integer id) {
    model.addAttribute("notesList", noteService.getNoteList(id));
    model.addAttribute("patientData", patientService.getPatient(id));
    model.addAttribute("patientReport", reportService.getReportByPatientId(id));

    return "note/note";
  }

  @GetMapping("/note/add/{id}")
  public String addNote(Model model, @PathVariable Integer id) {
    model.addAttribute("patientId", id);

    return "note/noteAdd";
  }

  @PostMapping("/note/validate/{id}")
  public String validate(@RequestParam("note") String note, @PathVariable Integer id) {
    noteService.createNote(note, id);

    return "redirect:/patient";
  }
}
