package com.medilabo.front.controllers;

import com.medilabo.front.entities.Patient;
import com.medilabo.front.services.PatientService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PatientController {

  @Autowired
  private PatientService patientService;

  @RequestMapping("/patient")
  public String homePatient(Model model) {
    model.addAttribute("patientList", patientService.getPatientList());
    return "patient/patient";
  }

  @GetMapping("/patient/add")
  public String addPatient(Patient patient) {
    return "patient/patientAdd";
  }

  @PostMapping("/patient/validate/create")
  public String validateCreate(@Valid Patient patient) {
    patientService.createPatient(patient);

    return "redirect:/patient";
  }

  @GetMapping("/patient/update/{id}")
  public String updatePatient(Patient patient, @PathVariable("id") Integer id, Model model) {

    model.addAttribute("patientData", patientService.getPatient(id));

    return "patient/patientUpdate";
  }

  @PostMapping("/patient/validate/update/{id}")
  public String validateUpdate(@Valid Patient patient, @PathVariable("id") Integer id) {
    patient.setId(id);
    patientService.updatePatient(patient);

    return "redirect:/patient";
  }

  @GetMapping("/patient/delete/{id}")
  public String deletePatient(@PathVariable("id") Integer id) {
    patientService.deletePatient(id);
    return "redirect:/patient";
  }
}
