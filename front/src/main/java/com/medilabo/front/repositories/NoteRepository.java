package com.medilabo.front.repositories;

import com.medilabo.front.entities.Note;
import com.medilabo.front.entities.Patient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Repository
public class NoteRepository {
  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${gateway.url}")
  private String gatewayUrl;

  public List<Note> getNoteList(Integer id) {
    return restTemplate.exchange(gatewayUrl + "/note/" + id, HttpMethod.GET, null, new ParameterizedTypeReference<List<Note>>() {}).getBody();
  }

  public void createNote(String note, Integer id) {
    restTemplate.postForEntity(gatewayUrl + "/note/add", new Note(id, note), Patient.class);
  }
}
