package com.medilabo.front.repositories;

import com.medilabo.front.entities.Patient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Repository
public class PatientRepository {
    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${gateway.url}")
    private String gatewayUrl;

    public List<Patient> getPatientList() {
      return restTemplate.exchange(gatewayUrl + "/patient/data", HttpMethod.GET, null, new ParameterizedTypeReference<List<Patient>>() {}).getBody();
    }

    public void createPatient(Patient patient) {
      restTemplate.postForEntity(gatewayUrl + "/patient/add", patient, Patient.class);
    }

  public void deletePatient(Integer id) {
      restTemplate.delete(gatewayUrl + "/patient/delete/"+id);
  }

  public Patient getPatient(int patientId) {
    return restTemplate.exchange( gatewayUrl + "/patient/data/"+patientId, HttpMethod.GET, null, Patient.class).getBody();
  }

  public void updatePatient(Patient patient) {
      restTemplate.postForEntity(gatewayUrl + "/patient/update", patient, Patient.class);
  }
}
