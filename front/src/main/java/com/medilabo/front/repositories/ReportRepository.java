package com.medilabo.front.repositories;

import com.medilabo.front.entities.Patient;
import com.medilabo.front.entities.Report;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class ReportRepository {
  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${gateway.url}")
  private String gatewayUrl;

  public Report getReportByPatientId(int patientId) {
    return restTemplate.exchange(gatewayUrl + "/report/"+patientId, HttpMethod.GET, null, Report.class).getBody();
  }
}
