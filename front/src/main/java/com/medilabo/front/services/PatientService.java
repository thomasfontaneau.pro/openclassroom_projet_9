package com.medilabo.front.services;

import com.medilabo.front.entities.Patient;
import com.medilabo.front.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

  @Autowired
  public PatientRepository patientRepository;

  public List<Patient> getPatientList() {
    return patientRepository.getPatientList();
  }

  public void createPatient(Patient patient) {
    patientRepository.createPatient(patient);
  }

  public void deletePatient(Integer id) {
    patientRepository.deletePatient(id);
  }

  public Patient getPatient(int patientId) {
    return patientRepository.getPatient(patientId);
  }

  public void updatePatient(Patient patient) {
    patientRepository.updatePatient(patient);
  }
}
