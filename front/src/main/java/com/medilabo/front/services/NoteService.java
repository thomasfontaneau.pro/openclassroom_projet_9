package com.medilabo.front.services;

import com.medilabo.front.entities.Note;
import com.medilabo.front.repositories.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {
  @Autowired
  public NoteRepository noteRepository;

  public List<Note> getNoteList(Integer id) {
    return noteRepository.getNoteList(id);
  }

  public void createNote(String note, Integer id) {
    noteRepository.createNote(note, id);
  }

}
