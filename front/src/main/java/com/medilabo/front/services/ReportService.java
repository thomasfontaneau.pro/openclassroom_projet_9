package com.medilabo.front.services;

import com.medilabo.front.entities.Report;
import com.medilabo.front.repositories.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class ReportService {

  @Autowired
  private ReportRepository reportRepository;

  public Report getReportByPatientId(int patientId) {
    return reportRepository.getReportByPatientId(patientId);
  }
}
