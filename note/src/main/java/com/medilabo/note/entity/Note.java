package com.medilabo.note.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

public class Note {
  @Id
  private String id;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private LocalDate date;

  private Integer patientId;

  private String note;

  public Note(String id, LocalDate date, int patientId, String note) {
    this.id = id;
    this.date = date;
    this.patientId = patientId;
    this.note = note;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Integer getPatientId() {
    return patientId;
  }

  public void setPatientId(Integer patientId) {
    this.patientId = patientId;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
