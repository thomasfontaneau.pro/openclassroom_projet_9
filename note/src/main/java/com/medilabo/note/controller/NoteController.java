package com.medilabo.note.controller;

import com.medilabo.note.service.NoteService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.medilabo.note.entity.Note;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
public class NoteController {

  @Autowired
  private NoteService noteService;

  @PostMapping("/note/add")
  @ResponseBody
  public void addNote(@Valid @RequestBody Note note) {
    noteService.addNote(note);
  }

  @GetMapping("/note/{id}")
  @ResponseBody
  public List<Note> getNoteByPatientId(@PathVariable("id") Integer id) {
    return noteService.getNoteByPatientId(id);
  }
}
