package com.medilabo.note.service;

import com.medilabo.note.entity.Note;
import com.medilabo.note.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class NoteService {

  //Todo ajouter une verif si le patient existe

  @Autowired
  private NoteRepository noteRepository;

  private static String generateRandomId() {
    return UUID.randomUUID().toString();
  }

  public void addNote(Note note) {
    noteRepository.save(new Note(generateRandomId(), LocalDate.now(), note.getPatientId(), note.getNote()));
  }

  public List<Note> getNoteByPatientId(int id) {
    return noteRepository.findByPatientId(id);
  }
}
