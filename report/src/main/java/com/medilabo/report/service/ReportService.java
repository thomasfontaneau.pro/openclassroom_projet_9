package com.medilabo.report.service;

import com.medilabo.report.entity.Note;
import com.medilabo.report.entity.PersonalData;
import com.medilabo.report.entity.Report;
import com.medilabo.report.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ReportService {
  private static final List<String> triggers = Arrays.asList("hémoglobine a1c", "microalbumine", "taille", "poids", "fumeur", "fumeuse", "anormal", "cholestérol", "vertiges", "rechute", "réaction", "anticorps");

  @Autowired
  private ReportRepository reportRepository;

  public List<Note> getNotes(int patientId) {
    return reportRepository.getPatientNote(patientId);
  }

  public PersonalData getPersonalData(int patientId) {
    return reportRepository.getPersonalData(patientId);
  }

  public Report getReport(int patientId) {
    List<Note> notes = reportRepository.getPatientNote(patientId);
    PersonalData personalData = reportRepository.getPersonalData(patientId);

    int old = Period.between(personalData.getBirthday(), LocalDate.now()).getYears();
    int nbTrigger = countTriggerUsage(notes);

    if (old > 30) {
      return switch (nbTrigger) {
        case 0, 1 -> Report.NONE;
        case 2, 3, 4, 5 -> Report.BORDERLINE;
        case 6, 7 -> Report.IN_DANGER;
        default -> Report.EARLY_ONSET;
      };
    } else {
      if (personalData.getGender() == 'M') {
        return switch (nbTrigger) {
          case 0, 1, 2 -> Report.NONE;
          case 3, 4 -> Report.IN_DANGER;
          default -> Report.EARLY_ONSET;
        };
      } else {
        return switch (nbTrigger) {
          case 0, 1, 2, 3 -> Report.NONE;
          case 4, 5, 6 -> Report.IN_DANGER;
          default -> Report.EARLY_ONSET;
        };
      }
    }
  }

  public static int countTriggerUsage(List<Note> notes) {
    int totalCount = 0;

    for (Note note : notes) {
      for (String trigger : triggers) {
        if (note.getNote().toLowerCase().contains(trigger.toLowerCase())) {
          totalCount += 1;
        }
      }
    }
    return totalCount;
  }
}
