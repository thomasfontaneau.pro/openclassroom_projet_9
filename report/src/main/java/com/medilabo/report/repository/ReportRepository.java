package com.medilabo.report.repository;

import com.medilabo.report.entity.Note;
import com.medilabo.report.entity.PersonalData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Repository
public class ReportRepository {
  private final RestTemplate restTemplate = new RestTemplate();

  @Value("${note.url}")
  private String noteUrl;

  @Value("${patient.url}")
  private String patientUrl;

  public List<Note> getPatientNote(int patientId) {
    return restTemplate.exchange(noteUrl + "/note/"+patientId, HttpMethod.GET, null, new ParameterizedTypeReference<List<Note>>() {}).getBody();
  }

  public PersonalData getPersonalData(int patientId) {
    return restTemplate.getForObject(patientUrl + "/patient/data/"+patientId, PersonalData.class);
  }
}
