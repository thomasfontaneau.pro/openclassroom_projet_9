package com.medilabo.report.controller;

import com.medilabo.report.entity.Note;
import com.medilabo.report.entity.Report;
import com.medilabo.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ReportController {
  @Autowired
  private ReportService reportService;

  @GetMapping("/get/{id}")
  @ResponseBody
  public List<Note> getNoteByPatientId(@PathVariable("id") Integer id) {
    return reportService.getNotes(id);
  }

  @GetMapping("/report/{id}")
  @ResponseBody
  public Report getReport(@PathVariable("id") Integer id) {
    return reportService.getReport(id);
  }
}
