package com.medilabo.report.entity;

public enum Report {
  NONE, BORDERLINE, IN_DANGER, EARLY_ONSET
}
