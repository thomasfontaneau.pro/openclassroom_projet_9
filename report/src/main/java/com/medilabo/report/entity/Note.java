package com.medilabo.report.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class Note {
  private String id;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private LocalDate date;

  private Integer patientId;

  private String note;

  public Note(String id, LocalDate date, int patientId, String note) {
    this.id = id;
    this.date = date;
    this.patientId = patientId;
    this.note = note;
  }

}